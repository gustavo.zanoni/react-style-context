import * as React from 'react';

export const styles = {
  old: "is-old-style",
  new: "is-new-style"
};

export const StyleContext = React.createContext(
  styles.old // default value
);
