import * as React from "react";
import "./App.css";

import { StyleContext } from "./StyleContext";

class SamplePage extends React.Component {
  render() {
    let style = this.context;
    return (
      <div className={style}>
        <span>Hello World!</span>
      </div>
    );
  }
}

SamplePage.contextType = StyleContext;
export default SamplePage;
