import * as React from 'react';
import './App.css';

import { StyleContext, styles } from "./StyleContext";
import SamplePage from "./SamplePage";

function App() {
  return (
    <StyleContext.Provider value={styles.new}>
      <SamplePage />
    </StyleContext.Provider>  
  );
}

export default App;
